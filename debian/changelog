ruby-columnize (0.9.0-2) UNRELEASED; urgency=medium

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Move source package lintian overrides to debian/source.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Apply multi-arch hints.
    + ruby-columnize: Add :all qualifier for ruby dependency.
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 03:48:07 +0530

ruby-columnize (0.9.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 09 Jun 2015 11:17:43 -0300

ruby-columnize (0.8.9-1) unstable; urgency=low

  * Team upload.

  [ Cédric Boutillier ]
  * debian/control:
    - remove obsolete DM-Upload-Allowed flag
    - use canonical URI in Vcs-* fields

  [ Nitesh A Jain ]
  * Imported Upstream version 0.8.9
  * Bumped standards to 3.9.5
  * Bumped gem2deb dependency to 0.7.5
  * d/copyright: for copyright format 1.0

  [ Jonas Genannt ]
  * d/copyright:
    - fixed Source url
    - fixed format for copyright 1.0
  * d/control:
    - bumped up standards version to 3.9.6 (no changed needed)
    - added testsuite header
  * d/ruby-tests.rb: removed whitespace
  * d/rules: do not install Makefiles to package
  * added patch to fix require for autopkg tests

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Fri, 10 Oct 2014 16:37:34 +0200

ruby-columnize (0.3.6-2) unstable; urgency=low

  * Team upload
  * Change Vcs filed to team repo
  * Cleanup ruby-tests.rb

  [Nandaja Verma]
  * Update description in debian/control
  * Update debian/copyright

 -- Praveen Arimbrathodiyil <praveen@debian.org>  Thu, 01 Nov 2012 12:41:43 +0530

ruby-columnize (0.3.6-1) unstable; urgency=low

  * Initial release (Closes: #691603)

 -- Nandaja Varma <nandaja.varma@gmail.com>  Mon, 29 Oct 2012 14:49:07 +0400
